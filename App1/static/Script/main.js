function bookSearch() {
    var search = document.getElementById('search').value
    document.getElementById('results').innerHTML = ""
    console.log(search)

    $.ajax ({
        url:"https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",

        success: function(data) {
            for(i = 0; i < 10; i++) {
                results.innerHTML += '<tr><th scope="row">' +  "<img src='" + data.items[i].volumeInfo.imageLinks.thumbnail + "'></img></th><td>" + data.items[i].volumeInfo.title + '</td><td>' + data.items[i].volumeInfo.authors[0] + '</td><td>' + data.items[i].volumeInfo.publishedDate + '</td><td>' + data.items[i].volumeInfo.description + '</td><td>' + '<div class="heart">' + '<i class="fa fa-heart-o" aria-hidden="true" ></i>' + '</div>' + '</td></tr>'
            }
            $('.heart').on('click', function () {
                if ($(this).children().hasClass('fa-heart-o')) {
                    $(this).children().removeClass('fa-heart-o');
                    $(this).children().addClass('fa-heart');
                } else {
                    $(this).children().removeClass('fa-heart');
                    $(this).children().addClass('fa-heart-o');
                }
            })
        },

        type: 'GET'
    });
}

document.getElementById('button').addEventListener('click', bookSearch, false)