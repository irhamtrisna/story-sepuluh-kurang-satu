from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import unittest

# Create your tests here.
class UnitTest(TestCase):
    def test_app_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_app_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_app_using_main_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main.html')

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url + '/')
        super(FunctionalTest, self).setUp()

    def test_search_book(self):
        selenium = self.selenium

        selenium.implicitly_wait(5)
        searchInput = selenium.find_element_by_id('search')
        searchInput.send_keys('Harry Potter')
        searchButton = selenium.find_element_by_id('button')
        searchButton.click()

        selenium.implicitly_wait(5)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()
        